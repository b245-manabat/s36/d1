// packages
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./Routes/taskRoute.js")

// server
const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch245-manabat.mca9yeu.mongodb.net/s35-discussion?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		})

	// check connection
	let db = mongoose.connection;
		// error catcher
		db.on("error", console.error.bind(console, "Connection Error!"));
		// confirmation of connection
		db.once("open", ()=>console.log("We are now connected to the cloud!"));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// routing
app.use("/tasks", taskRoute);


app.listen(port, ()=>console.log(`Server is runnign at port ${port}`));

/*
	Separations of concerns:
	Model should be connected to the controller.
	Controller should be connected to the Routes.
	Route should be connected to the server/application.
*/