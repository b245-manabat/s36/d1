const express = require("express");
const router = express.Router()

const taskController = require("../Controllers/taskController.js");


/*
	Routes
*/

// route for getAll
router.get("/get",taskController.getAll)

// route for createTask
router.post("/addTask",taskController.createTask);

// route for deleteTask
router.delete("/deleteTask/:id",taskController.deleteTask);


module.exports = router;