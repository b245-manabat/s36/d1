const Task = require("../Models/task.js");

/*
	Controllers and functions
*/

// controller/function to get all the task on our database
module.exports.getAll = (request, response) => {

	Task.find({})
	// to capture the result of find method
	.then(result=>{
		return response.send(result)
	})
	// to catch method capture the error when find method is executed
	.catch(error => {
		return response.send(error);
	})
}

// add task on our database
module.exports.createTask = (request, response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null) {
			return response.send("The task is already existing!")
		}
		else {
			let newTask = new Task({
				name: input.name
			});

			newTask.save().then(save => {
				return response.send("The task is successfully added!")
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error)
	})
};

// controller that will delete the controller that contains the given object id
module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - to find the document that contains the id and then delete the document
	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
};